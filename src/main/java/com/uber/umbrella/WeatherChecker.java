package com.uber.umbrella;


import android.content.Context;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;


public class WeatherChecker implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    private GoogleApiClient mGoogleApiClient;
    private static final String WEATHER_URL = "http://api.openweathermap.org/data/2.5/forecast/daily?lat=%f&lon=%f&mode=json&units=metric&cnt=1";
    private OnCompleteListener onCompleteListener;

    public WeatherChecker(Context context, OnCompleteListener onCompleteListener) {
        this.onCompleteListener = onCompleteListener;
        mGoogleApiClient = new GoogleApiClient.Builder(context)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnected(Bundle bundle) {
        Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (location != null) {
            new WebServiceTask().execute(location.getLatitude(), location.getLongitude());
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
    }

    public interface OnCompleteListener {
        void onComplete(Boolean shouldTakeUmbrella);
    }


    private class WebServiceTask extends AsyncTask<Double, Integer, Boolean> {

        @Override
        protected Boolean doInBackground(Double... params) {
            Boolean result = null;
            HttpURLConnection urlConnection = null;

            try {
                URL url = new URL(String.format(WEATHER_URL, params[0], params[1]));
                urlConnection = (HttpURLConnection) url.openConnection();
                result = useUmbrella(urlConnection.getInputStream());
            } catch (IOException e) {
                Log.e("WeatherChecker", "Error ", e);
            } finally {
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
            }

            return result;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            onCompleteListener.onComplete(result);
        }

        private Boolean useUmbrella(InputStream in) {
            StringBuilder stringBuilder = new StringBuilder();
            BufferedReader bufferedReader = null;

            try {
                bufferedReader = new BufferedReader(new InputStreamReader(in));
                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    stringBuilder.append(line + "\n");
                }
                Log.i("Returned data", stringBuilder.toString());

                JSONObject forecastJson = new JSONObject(stringBuilder.toString());
                JSONArray weatherArray = forecastJson.getJSONArray("list");
                JSONObject todayForecast = weatherArray.getJSONObject(0);
                return todayForecast.has("rain") || todayForecast.has("snow");
            } catch (Exception e) {
                Log.e("WeatherChecker", "Error", e);
            } finally {
                if (bufferedReader != null) {
                    try {
                        bufferedReader.close();
                    } catch (final IOException e) {
                        Log.e("WeatherChecker", "Error closing stream", e);
                    }
                }
            }

            return null;
        }

    }

}
