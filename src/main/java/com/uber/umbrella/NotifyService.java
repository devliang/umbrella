package com.uber.umbrella;

import android.app.IntentService;
import android.content.Intent;


public class NotifyService extends IntentService {

    public NotifyService() {
        super("NotifyService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        final NotifyService that = this;
        new WeatherChecker(this, new WeatherChecker.OnCompleteListener() {
            @Override
            public void onComplete(Boolean shouldTakeUmbrella) {
                if (shouldTakeUmbrella != null && shouldTakeUmbrella) {
                    new Notifier().createNotification(that, "Umbrella", "You should take an umbrella today!");
                }
            }
        });
        AlarmBroadcastReceiver.completeWakefulIntent(intent);
    }

}
